﻿/*
 *
 * Common Validation Framework, version 0.1
 *  (c) 2006-2007 Demo2do
 *
 *  For details, see the Demo2do web site: http://www.demo2do.com/
 *
 * @author keen.jiang
 *
/*--------------------------------------------------------------------------*/

/**
 * define package name
 */
if (demo2do == undefined) var demo2do = {};

demo2do.validator = {};

/**
 * Main Class, used to bind listener with inputboxes & forms
 * and check all the vaildars when one form is submitted.
 */
demo2do.validator.Manager = Class.create();
demo2do.validator.Manager.prototype = {

  Version: '0.1',

  /**
   * stores forms which has input field need to validate
   */
  forms: $H({}),

  /**
   * register all validators provided by the framework
   */
  sysValidators: $A({}),

  initialize : function(){
      Event.observe(window, 'load',  this.register.bindAsEventListener(this));
  },

  /**
   * detect fields need validate, and bind listeners with them
   */
  register: function() {

    /**
     * iterate all the forms in the document, find all the inputboxes ask for validation
     */
    $A(document.forms).each(function (form){

        /**
         * find all inputboxes under that form and iterate them
         */
        $A(form.getElementsByTagName("input")).each(function(inputbox){

            //find all inputboxes which has "validate" attribute specified
            var allexpressionstr = inputbox.getAttribute("validate") ||inputbox.getAttribute("check") ||inputbox.getAttribute("v") ;
            if(allexpressionstr != null){

                //iterate all expressions
                allexpression = allexpressionstr.split(";");

                allexpression.each(function(expression){

                    expression = expression.trim();

                    //iterate all registered validators, match them with expression specified by the inputbox,
                    //bind those matched inputbox with listener of validator
                    var anyValidatorFound = false;
                    this.sysValidators.each(function(sysvalidator){

                        //check if the validator is defined in the expression
                        if(sysvalidator.match(expression)){

                            //save matched validator into the inputbox
                            if(inputbox.validatorController == undefined){
                                var controller = new demo2do.validator.Controller();
                                inputbox.validatorController = controller;

                                //bind listener on inputbox
                                Event.observe(inputbox, 'blur', controller.check.bindAsEventListener(controller, inputbox));
                            }

                            inputbox.validatorController.addValidator(sysvalidator.getInstance(expression, inputbox));

                            anyValidatorFound = true;
                            throw $break;
                        }
                    });

                    //check if any validator is found
                    if(anyValidatorFound)
                    {
                        //store the form into the Hash if it's not there before
                        if(this.forms[form.id] == undefined) {

                        this.forms[form.id] = new Array();
                         //bind the listener of onsubmit with the form
                        Event.observe(form, 'submit', this.checkFormValidation.bindAsEventListener(this, form));
                        }

                        //push the inputbox into the inputbox array of the form
                        this.forms[form.id].push(inputbox);


                    }
                }.bind(this));
            }
        }.bind(this));
    }.bind(this));

  },

  /**
   *
   * @param {Object} event
   * @param {Object} form
   */
  checkFormValidation: function (event, form){

    var isValidated = true;

    //check all the form
    var inputboxes = this.forms[form.id];

    inputboxes.each(function(inputbox){
        var vc= inputbox.validatorController;
        if(!vc.check.bind(vc, event, inputbox)())
           isValidated = false;
    });

    if(!isValidated)
        Event.stop(event);

  },

  /**
   *
   */
  registerSysValidator : function( validator){
    this.sysValidators.push(validator);
  }

};

/**
 * validator Controller
 */
demo2do.validator.Controller = Class.create();
demo2do.validator.Controller.prototype = {

    initialize : function(){

    },

    check: function(e, inputbox){

        var isValidated = true;
        var message = "";

        //iterate all the validator against the inputbox
        this.validators.each(function(validator){

            if(!validator.validate()){
                message += validator.message + "\n";
                isValidated = false;
            }
        });

        if(!isValidated){
            //notify user here
            inputbox.className = "notvalidedtext";
            inputbox.title = message;
            return false;
        }else{
            inputbox.className = "text";
            inputbox.title = "";
            return true;
        }
    },

    addValidator : function(validator){
        if(this.validators == undefined) this.validators = new Array();
        this.validators.push(validator);
    }
};


//********************************************************************
// validator part
//********************************************************************

//TODO: put the parse part to the initialization function

/**
 * Validator checks if the inputbox is null
 */
demo2do.validator.RequiredValidator = Class.create();
demo2do.validator.RequiredValidator.prototype =  {

    name : "RequiredValidator",

    initialize : function(experssion, inputbox){

        if(inputbox != null){
            this.expression = experssion;
            this.message = "这是必填内容，请输入。";
            this.inputbox = inputbox;
        }
    },

    match : function(expression){;return /required|req/i.test(expression)},

    validate : function(){

        return !Object.isBlank(this.inputbox.value);
    },

    getInstance : function(expression, inputbox){

        return new demo2do.validator.RequiredValidator(expression, inputbox);
    }
};

/**
 * Validator compare with input value
 */
demo2do.validator.CompareValidator = Class.create();
demo2do.validator.CompareValidator.prototype =  {

    name : "CompareValidator",

    initialize : function(experssion, inputbox){

        if(inputbox != null){

            this.expression = experssion;
            this.message = "CompareValidatorError";
            this.inputbox = inputbox;
            this.parseExpression();
        }
    },

    match : function(expression){ return /^(>|>=|<|<=|=)\s*[0-9]*$/.test(expression)},

    parseExpression : function(){

        this.symbol = this.expression.match(/^(>=|>|<=|<|=)/)[0];
        this.number = this.expression.match(/[0-9]*$/)[0];
    },

    validate : function(){

        var inputvalue = parseFloat(this.inputbox.value);

        if(!/^\d*$/.test(this.inputbox.value)) {
            this.message = "请输入数字。";
            return false;
        }

        var number = this.number;
        switch(this.symbol) {
            case '>=':   this.message = "it must be bigger than or equal to " + number;  return inputvalue >= number;
            case '>':  this.message = "it must be bigger than " + number;  return inputvalue > number;
            case '=':   this.message = "it must be equal to " + number; return inputvalue == number;
            case '<':   this.message = "it must be less than " + number; return inputvalue < number;
            case '<=':   this.message = "it must be bigger than or equal to " + number; return inputvalue <= number;
        }
    },

    getInstance : function(expression, inputbox){
        return new demo2do.validator.CompareValidator(expression, inputbox);
    }
};

/**
 * Validator checkes range
 */
demo2do.validator.RangeValidator = Class.create();
demo2do.validator.RangeValidator.prototype =  {

    name : "RangeValidator",

    initialize : function(experssion, inputbox){

        if(inputbox != null){

            this.expression = experssion;
            this.message = "RangeValidatorError";
            this.inputbox = inputbox;
            this.parseExpression();
        }
    },

    match : function(expression){ return /^[0-9]*\s*(\.\.|\.\.\.)\s*[0-9]*$/.test(expression)},

    parseExpression : function(){

        this.symbol = this.expression.match(/\.\.\.|\.\./)[0];
        this.number = this.expression.match(/[0-9]*$/)[0];

        this.l = this.expression.match(/^[0-9]*/)[0];
        this.r= this.expression.match(/[0-9]*$/)[0];
        this.isswitched = false;
        if(this.l > this.r) { var temp = l; this.l = this.r; this.r = temp; this.isswitched = true}
    },

    validate : function(){

        var inputvalue = parseFloat(this.inputbox.value);

        if(!/^\d*$/.test(inputvalue)) {
            this.message = "请输入数字。";
            return false;
        }

        var l = this.l;
        var r = this.r;

         switch(this.symbol) {

            case '..':   this.message = "输入数字必须在" + l + "到" + r + "之间。	";  return l <= inputvalue && inputvalue <=r;

            case '...':

                this.message = "输入数字必须在" + l + "到" + r + "之间。";

                if(!this.isswitched){
                     this.message +=", " + r + "不在范围内。";
                     return l <= inputvalue && inputvalue <r;
                }
                else{
                     this.message +=", " + l + "不在范围内。";
                     return l < inputvalue && inputvalue <=r;
                }
         }
    },

    getInstance : function(expression, inputbox){
        return new demo2do.validator.RangeValidator(expression, inputbox);
    }
};

/**
 * Validator checkes Internet Email
 */
demo2do.validator.EmailValidator = Class.create();
demo2do.validator.EmailValidator.prototype =  {

    name : "EmailValidator",

    initialize : function(experssion, inputbox){

        this.expression = experssion;
        this.message = "请输入合法的Email地址。";
        this.inputbox = inputbox;
    },

    match : function(expression){  return /email|mail/i.test(expression)},

    validate : function(){
        return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.inputbox.value);
    },

    getInstance : function(expression, inputbox){
        return new demo2do.validator.EmailValidator(expression, inputbox);
    }
};

/**
 * Validator checkes Number Type
 */
demo2do.validator.NumberTypeValidator = Class.create();
demo2do.validator.NumberTypeValidator.prototype =  {

    name : "NumberTypeValidator",

    initialize : function(experssion, inputbox){

        if(inputbox != null){

            this.expression = experssion;
            this.message = "请输入数字。";
            this.inputbox = inputbox;
            this.parseExpression();
        }
    },

    parseExpression : function(){

        this.digit = parseInt(this.expression.match(/[0-9]*$/)[0]);

    },
    match : function(expression){ return /^(number|num)(\s*\:\s*[0-9]*)?$/i.test(expression)},

    validate : function(){

        if(Object.isEmpty(this.inputbox.value)) return true;

        var inputvalue = parseFloat(this.inputbox.value);

        if(isNaN(this.digit)){
            return  /^\d*$/.test(this.inputbox.value);
        }else{
            if(!/^\d*$/.test(this.inputbox.value))
                return false;
            else{
                this.message = "输入数字只可以有" + this.digit + "位。"
                return  Math.pow(10, (this.digit-1)) <= inputvalue && inputvalue <Math.pow(10, this.digit);
            }
        }
    },

    getInstance : function(expression, inputbox){
        return new demo2do.validator.NumberTypeValidator(expression, inputbox);
    }
};

/**
 * Validator checkes Regular Expression
 */
demo2do.validator.RegularExpressionValidator = Class.create();
demo2do.validator.RegularExpressionValidator.prototype =  {

    name : "RegularExpressionValidator",

    initialize : function(experssion, inputbox){

        if(inputbox != null){

            this.expression = experssion;
            this.inputbox = inputbox;
            var description = this.inputbox.getAttribute("description");
            if(Object.isEmpty(description)){
                this.message = "输入内容不符合正则表达式：";
                this.useDescription = false;
            }else{
                this.message = description;
                this.useDescription = true;
            }

            this.parseExpression();
        }
    },

    match : function(expression){ return /^re\:/i.test(expression)},

    parseExpression : function(){
        this.regularexpression = this.expression.substring(3);
        if(!this.useDescription)
            this.message += this.regularexpression;
    },

    validate : function(){
        return (new RegExp(this.regularexpression)).test(this.inputbox.value);
    },

    getInstance : function(expression, inputbox){
        return new demo2do.validator.RegularExpressionValidator(expression, inputbox);
    }
};

/**
 * Validator checkes Date
 */
demo2do.validator.DateTypeValidator = Class.create();
demo2do.validator.DateTypeValidator.prototype =  {

    name : "DateTypeValidator",

    initialize : function(experssion, inputbox){

        if(inputbox != null){
            this.expression = experssion;
            this.inputbox = inputbox;
            this.message = "请输入合法日期。";
        }
    },

    match : function(expression){ return /date/i.test(expression)},

    validate : function(){
        var inputvalue = this.inputbox.value;
        inputvalue = inputvalue.trim();

        if(this.isValidDate(inputvalue, "YMD")){
            inputvalue = inputvalue.replace(/\.|\//g, "-");
            this.inputbox.value = this.format(inputvalue);
            return true;
        }else
            return false;
    },

    format: function(inputdate){
        var date = "";
        var pos = 0;
        inputdate= inputdate.gsub(/\d+/, function(match){
            match = "" + match;
            pos++;
            if(pos== 1){
                while(match.length<=3){
                 match = "0" + match;
                }
                return match;

            }else {
                if(match.length==1)
                   return "0" + match;
                else
                   return match;
            }
        }.bind(this));
        return inputdate;
    },

    isValidDate: function(dateStr, format) {

       if (format == null) { format = "MDY"; }
       format = format.toUpperCase();
       if (format.length != 3) { format = "MDY"; }
       if ( (format.indexOf("M") == -1) || (format.indexOf("D") == -1) || (format.indexOf("Y") == -1) ) { format = "MDY"; }
       if (format.substring(0, 1) == "Y") { // If the year is first
          var reg1 = /^\d{2}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
          var reg2 = /^\d{4}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
       } else if (format.substring(1, 2) == "Y") { // If the year is second
          var reg1 = /^\d{1,2}(\-|\/|\.)\d{2}\1\d{1,2}$/
          var reg2 = /^\d{1,2}(\-|\/|\.)\d{4}\1\d{1,2}$/
       } else { // The year must be third
          var reg1 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{2}$/
          var reg2 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/
       }
       // If it doesn't conform to the right format (with either a 2 digit year or 4 digit year), fail
       if ( (reg1.test(dateStr) == false) && (reg2.test(dateStr) == false) ) { return false; }
       var parts = dateStr.split(RegExp.$1); // Split into 3 parts based on what the divider was
       // Check to see if the 3 parts end up making a valid date
       if (format.substring(0, 1) == "M") { var mm = parts[0]; }
       else
          if (format.substring(1, 2) == "M") { var mm = parts[1]; } else { var mm = parts[2]; }
       if (format.substring(0, 1) == "D") { var dd = parts[0]; }
       else
          if (format.substring(1, 2) == "D") { var dd = parts[1]; } else { var dd = parts[2]; }
       if (format.substring(0, 1) == "Y") { var yy = parts[0]; }
        else
          if (format.substring(1, 2) == "Y") { var yy = parts[1]; } else { var yy = parts[2]; }
       if (parseFloat(yy) <= 50) { yy = (parseFloat(yy) + 2000).toString(); }
       if (parseFloat(yy) <= 99) { yy = (parseFloat(yy) + 1900).toString(); }
       var dt = new Date(parseFloat(yy), parseFloat(mm)-1, parseFloat(dd), 0, 0, 0, 0);
       if (parseFloat(dd) != dt.getDate()) { return false; }
       if (parseFloat(mm)-1 != dt.getMonth()) { return false; }
       return true;
    },

    getInstance : function(expression, inputbox){
        return new demo2do.validator.DateTypeValidator(expression, inputbox);
    }

};

/**
 * Validator checkes multiple fields
 */
demo2do.validator.ComboFieldsValidator = Class.create();
demo2do.validator.ComboFieldsValidator.prototype =  {

    name : "ComboFieldsValidator",

    initialize : function(experssion, inputbox){

        if(inputbox != null){

            this.expression = experssion;
            this.inputbox = inputbox;
            this.parseExpression();

        }
    },

    parseExpression : function(){
        //TODO: should we bind listener to both combo inputbox????

        var comboExpression = this.expression.substring(6);

        var allowedOperatorsReg = /(>=|>|<=|<|==|=){1}/;

        this.operator = comboExpression.match(allowedOperatorsReg)[0];


        var posOfOperator = comboExpression.search(allowedOperatorsReg);

        var leftexpression = comboExpression.substring(0, posOfOperator);
        var rightexpression = comboExpression.substring(posOfOperator + this.operator.length);

        if(Object.isBlank(leftexpression))
            this.leftobjects = new Array(this.inputbox);
        else{
            this.leftobjects = this.getElements(leftexpression.trim());
        }

        this.rightobjects = this.getElements(rightexpression.trim());

    },

    match : function(expression){ return /^combo\:\s*(\$|#)?\w*\s*(>=|>|<=|<|=){1}\s*(\$|#)?\w*\s*/i.test(expression)},

    validate : function(){

        var leftobjects = this.leftobjects;
        var rightobjects = this.rightobjects;
        var operator = this.operator;

        var isValidationFailed = true;
        operator = operator == "=" ? "==" : operator;

        leftobjects.each(function(leftobject){

            rightobjects.each(function(rightobject){
                var statement;

                if(Object.isEmpty(leftobject.value) || Object.isEmpty(rightobject.value))  throw $break;

                var lvalue = Object.isNumber(leftobject.value) ? leftobject.value : "\"" + leftobject.value + "\"";
                var rvalue = Object.isNumber(rightobject.value) ? rightobject.value : "\"" + rightobject.value + "\"";



                statement = lvalue + operator + rvalue;

                if(!eval(statement)){
                    this.message = this.inputbox.getAttribute("description");
                    isValidationFailed = false;
                    throw $break;
                }

            }.bind(this))
        }.bind(this));
        return isValidationFailed;
    },

    getElements : function(expression){

        if(expression.indexOf("$") > -1){
            var elements = $(expression.match(/\w[^#$]*$/)[0]);

            if(elements.toArray)
                return elements;
            else
                return new Array(elements);
        }
        return $A(document.getElementsByName(expression.match(/\w[^#$]$/)[0]));
    },

    getInstance : function(expression, inputbox){
        return new demo2do.validator.ComboFieldsValidator(expression, inputbox);
    }
};

/**
 * Validator checkes time format (hh:mm:ss)
 */
demo2do.validator.TimeValidator = Class.create();
demo2do.validator.TimeValidator.prototype =  {

    name : "TimeValidator",

    initialize : function(experssion, inputbox){

        if(inputbox != null){

            this.expression = experssion;
            this.inputbox = inputbox;
            this.message = "请输入正确的时间 格式(HH:mm:ss)";

        }
    },

    match : function(expression){ return /time/i.test(expression)},

    validate : function(){

        return Object.isEmpty(this.inputbox.value) || /^(2[0-3]|[0-1][0-9])\:[0-5][0-9]\:[0-5][0-9]$/.test(this.inputbox.value);
    },

    getInstance : function(expression, inputbox){
        return new demo2do.validator.TimeValidator(expression, inputbox);
    }
};

/**
 * initialize validator Manager
 */

var validatorManager = new demo2do.validator.Manager();
validatorManager.registerSysValidator(new demo2do.validator.RequiredValidator());
validatorManager.registerSysValidator(new demo2do.validator.CompareValidator());
validatorManager.registerSysValidator(new demo2do.validator.RangeValidator());
validatorManager.registerSysValidator(new demo2do.validator.EmailValidator());
validatorManager.registerSysValidator(new demo2do.validator.NumberTypeValidator());
validatorManager.registerSysValidator(new demo2do.validator.RegularExpressionValidator());
validatorManager.registerSysValidator(new demo2do.validator.DateTypeValidator());
validatorManager.registerSysValidator(new demo2do.validator.ComboFieldsValidator());
validatorManager.registerSysValidator(new demo2do.validator.TimeValidator());


//datetime
//pattern: yyyy-mm-dd 00:00:00(default)
//validator expression: datetime(case insensitive)


//FIXME:
//4. =/==
//TODO:, ==3