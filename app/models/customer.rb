﻿class Customer < ActiveRecord::Base
    validates_format_of :mobile,
    :with => /^(13[0-9]|15[0|3|6|7|8|9]|18[05-9])\d{8}$/,
    :message => "请输入正确的手机号码",
    :if => Proc.new {|u| !u.mobile.blank?}
end
