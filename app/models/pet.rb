﻿class Pet < ActiveRecord::Base
  require 'mini_magick'
  
  belongs_to :customer

  attr_accessor :pic_name
  attr_accessor :pic_type
  attr_accessor :pic_data
  attr_accessor :x1, :y1, :x2, :y2

  #validate :valid_image_format
  validates_length_of :comment, :maximum => 70
  #validates_format_of :pic_type,
  #                    :with => /^image/,
  #                    :message => "--- 请选择正确的图片类型"

  #def uploaded_picture=(picture_field)
  #  self.image_url = move_file(picture_field)
  #  self.pic_type = picture_field.content_type.chomp
  #end

  def base_part_of(file_name)
    File.basename(file_name).gsub(/[^\w._-]/, '' )
  end

  def save_pic(file)
    save_name = "ABD_" + Time.new.to_i.to_s+".jpg"
    #保存图片的绝对路径
    path = "#{Rails.root}/public/upload/pet_pic/#{save_name}"

    File.open(path, "wb") do |f|
      f.write(file.read)
    end
    
    self.image_url = save_name
    self.pic_type = "image/jpg";
  end

  def move_file(file)
    #取得上传的文件名
    @name = base_part_of(file.original_filename)
    #使用当前经过的秒数加上源文件后缀名，作为保存的新名称。
    save_name = @name.split(".")[0] + Time.new.to_i.to_s+"."+@name.split(".")[1]
    #保存图片的绝对路径
    path = "#{Rails.root}/public/upload/pet_pic/#{save_name}"

    File.open(path, "wb") do |f|
      f.write(file.read)
    end
    save_name
  end

  def photo_resize
    image = MiniMagick::Image.open("#{Rails.root}/public/upload/pet_pic/#{self.image_url}")
    image.resize "320"

    width = self.x2.to_i - self.x1.to_i
    height= self.y2.to_i - self.y1.to_i

    image.crop "#{width}x#{height}+#{self.x1}+#{self.y1}"
    image.write  "#{Rails.root}/public/upload/pet_pic/re_#{self.image_url}"

    self.image_url = "re_#{self.image_url}"
    self.pic_type = "image"
  end

  def gen_pin
    "ABD%05d" % self.id
  end

  def deleted?
    deleted_at
  end
  
  protected
  def valid_image_format
    unless self.pic_type.to_s =~ /^image/
      errors.add(:uploaded_picture, '--- 请选择正确的图片类型' )
    end unless self.pic_type.nil?
  end
  
end
