﻿#coding: utf-8
class CustomersController < ApplicationController

  # GET /customers/new
  # GET /customers/new.xml
  def new
    @customer = Customer.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @customer }
    end
  end

  # POST /customers
  # POST /customers.xml
  def create
    @customer = Customer.new(params[:customer])

    @exist_cust = Customer.find_by_mobile(@customer.mobile)
    if @exist_cust.nil?
      respond_to do |format|
        if @customer.save
          flash[:notice] = '用户资料已经记录，快来给你的宠物起名字。'
          format.html { redirect_to :controller => :pets, :action => :new, :customer => @customer}
          format.xml  { render :xml => @customer, :status => :created, :location => @customer }
        else
          format.html { render :action => "new" }
          format.xml  { render :xml => @customer.errors, :status => :unprocessable_entity }
        end
      end
    else
      @customer = @exist_cust
      flash[:notice] = '用户资料已经记录，快来给你的宠物起名字。'
      redirect_to :controller => :pets, :action => :new, :customer => @customer
    end

  end


end
