﻿class PetsController < ActionController::Base

  layout "pets" , :except => [ :show ]

  # GET /pets
  # GET /pets.xml
  def index
    @pets = Pet.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @pets }
    end
  end

  # GET /pets/1
  # GET /pets/1.xml
  def show
    @pet = Pet.find(params[:id])
    @pet.customer = Customer.find(@pet.customer_id)
    @tpl_id = params[:tpl_id]
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @pet }
    end
  end


  # GET /pets/new
  # GET /pets/new.xml
  def new
    @pet = Pet.new
    @owner_id = params[:customer]
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @pet }
    end
  end

  # GET /pets/1/edit
  def edit
    @pet = Pet.find(params[:id])
  end

  # POST /pets
  # POST /pets.xml
  def create
    @pet = Pet.new(params[:pet])
    
    respond_to do |format|
      if @pet.save
        flash[:notice] = '宠物资料输入完成，让我们来拍照吧，把最美的瞬间留下。'
        format.html { redirect_to :action => 'camera', :id => @pet }
        format.xml  { render :xml => @pet, :status => :created, :location => @pet }
      else
        format.html { render :action => 'new' }
        format.xml  { render :xml => @pet.errors, :status => :unprocessable_entity }
      end
    end
  end

  def camera
    @pet = Pet.find(params[:id])
    respond_to do |format|
      format.html # carema.html.erb
      format.xml  { render :xml => @pet }
    end
  end

  def photo
    @pet = Pet.find(params[:id])
    @pet.customer = Customer.find(@pet.customer_id)

    respond_to do |format|
      format.html # photo.html.erb
      format.xml  { render :xml => @pet }
    end
  end

  def capture
    @pet = Pet.find(params[:id])
    @pet.save_pic(request.body)
    if @pet.save
      flash[:notice] = '拍照完成，选择你喜欢样式打印吧。'
      #response.write("fileurl="&replace(path,"\","\\"))
      render :text => "fileurl=/pets/" + @pet.id.to_s  + "/card_tpl/"
    else
      render :text => "fileurl=/pets/" + @pet.id.to_s  + "/card_tpl/"
    end
  end

  def photo_resize
    @pet = Pet.find(params[:pet_id])
    
    @pet.x1 = params[:x1]
    @pet.x2 = params[:x2]
    @pet.y1 = params[:y1]
    @pet.y2 = params[:y2]

    @pet.photo_resize

    if @pet.save
      flash[:notice] = 'Cool，裁剪完成，选择你喜欢样式打印吧。'
      respond_to do |format|
        format.html { render :template => 'pets/card_tpl', :id => @pet }
      end
      #redirect_to(@pet)
    else
      render :action => "new"
    end
  end

  def choose_tpl
    @pet = Pet.find(params[:id])
    @tpl_id = params[:tpl_id]
    redirect_to :action => 'show', :id => @pet, :tpl_id => @tpl_id
    #redirect_to(@pet)
  end

  # PUT /pets/1
  # PUT /pets/1.xml
  def update
    @pet = Pet.find(params[:id])

    respond_to do |format|
      if @pet.update_attributes(params[:pet])
        flash[:notice] = 'Pet was successfully updated.'
        format.html { redirect_to(@pet) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @pet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /pets/1
  # DELETE /pets/1.xml
  def destroy
    @pet = Pet.find(params[:id])
    @pet.destroy

    respond_to do |format|
      format.html { redirect_to(pets_url) }
      format.xml  { head :ok }
    end
  end

  def find
    #TODO
    render "pets/find"
  end

  def card_tpl
    #TODO
  end

  def search
    @keyword = params[:keyword].strip

    if (not @keyword.length == 8) or (not @keyword =~ /[ABD|abd]\d{5}/)
      flash[:notice] = '请输入正确的编号 例如：ABD00001'
      redirect_to :action => 'find'
    else
      flash[:notice] = nil
      id_number = @keyword[3, 5].to_i
      if not id_number == 0
        @pet = Pet.find_by_id(id_number)
        if @pet.nil?
          flash[:notice] = '未找到该身份卡，请重新搜索'
          redirect_to :action => 'find'
        else
          #render :template => 'pets/card_tpl', :id => @pet.id
          redirect_to :action => 'card_tpl', :id => @pet.id
        end
      else
        flash[:notice] = '请输入正确的编号'
        redirect_to :action => 'find'
      end
    end

  end
end
