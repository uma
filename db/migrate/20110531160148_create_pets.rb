class CreatePets < ActiveRecord::Migration
  def self.up
    create_table :pets do |t|
      t.string :name
      t.date :birthday
      t.integer :height
      t.integer :weight
      t.string :color
      t.string :eyecolor
      t.text :comment
      t.integer :customer_id
      t.string :pin
      t.string :image_url
      t.datetime :deleted_at

      t.timestamps
    end
  end

  def self.down
    drop_table :pets
  end
end
