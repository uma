class CreateCustomers < ActiveRecord::Migration
  def self.up
    create_table :customers do |t|
      t.string :name
      t.string :mobile
      t.string :email
      t.string :password
      t.string :city
      t.string :state
      t.string :phone
      t.string :nick_name
      t.string :image_url
      t.date    :birthday

      t.timestamps
    end
  end

  def self.down
    drop_table :customers
  end
end
